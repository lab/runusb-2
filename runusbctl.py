import zmq
from pathlib import Path
import sys
import time

context = zmq.Context()

socket = context.socket(zmq.PUB)
socket.connect("ipc://" + str(Path.cwd() / "runusb.sock"))

# time.sleep(3600)

socket.send_multipart([sys.argv[1].encode(), b"foo"])
