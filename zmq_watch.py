import zmq
from pathlib import Path

context = zmq.Context()

socket = context.socket(zmq.SUB)
socket.setsockopt_string(zmq.SUBSCRIBE, "log")
socket.connect("ipc://" + str(Path.cwd() / "runusb.sock"))

while True:
    print(socket.recv_multipart())
