import time
from itertools import count

for i in count():
    print("Still running", i)
    time.sleep(1)
